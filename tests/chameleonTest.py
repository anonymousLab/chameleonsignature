# exec with python3 -m tests.chameleonTest

from chameleon.chameleon import Chameleon
from chameleon.RSA import RSA
from chameleon.messageEncoder import MessageEncoder

def main():
  print("chameleon is on the way.")
  m = "chameleon"

  testChameleon = Chameleon()
  chameleonKey = testChameleon.generateKeyPairs()
  # print(chameleonKey)

  testRSA = RSA()
  RSAKey = testRSA.generateKeyPairs()
  # print(RSAKey)

  testME = MessageEncoder()
  encodedM = testME.encodeOAEP(m)

  signature = testChameleon.sign(RSAKey["sk"], RSAKey["vk"]["n"],
                                 chameleonKey["hk"], chameleonKey["params"],
                                 encodedM, None)
  print(signature)

  result = testChameleon.verify(RSAKey["vk"],
                                chameleonKey["hk"], chameleonKey["params"],
                                signature)
  # print(result)

  newMessage = "chameleonchameleon"
  newEncodedM = testME.encodeOAEP(newMessage)
  collision = testChameleon.collide(chameleonKey["ck"], chameleonKey["params"],
                                    signature, newEncodedM)
  print(collision)

  newSig = testChameleon.sign(RSAKey["sk"], RSAKey["vk"]["n"],
                              chameleonKey["hk"], chameleonKey["params"],
                              collision["_m"], collision["_r"])
  print(newSig)

if __name__ == "__main__":
  main()