# exec with python3 -m tests.RSATest

from chameleon.RSA import RSA as rsa

def main():
  testRSA = rsa()
  m = "chameleon"
  keyPair = testRSA.generateKeyPairs()
  print(keyPair)
  signature = testRSA.sign(keyPair["sk"], keyPair["vk"]["n"], m)
  print("signature:", signature)
  verified = testRSA.verify(keyPair["vk"], signature, m)
  print("verified:", verified)

if __name__ == "__main__":
  main()