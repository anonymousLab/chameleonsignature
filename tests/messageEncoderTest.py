# exec with python3 -m tests.chameleonTest

from chameleon.messageEncoder import MessageEncoder

def main():
  m = "chameleon"

  testOAEP = MessageEncoder()
  print("OAEP padding is on the way.")
  paddedM = testOAEP.encodeOAEP(m)
  print(paddedM)
  recoveredM = testOAEP.decodeOAEP(paddedM)
  print(recoveredM)

if __name__ == "__main__":
  main()