import os
import random
from hashlib import sha256

from chameleon.utilities import Utilities
from chameleon.randomness import RandomNumber
from chameleon.mathematics import Mathematics



class RSA:
  def __init__(self):
    self.rn = RandomNumber()
    self.utils = Utilities()
    self.math = Mathematics()

  def generateKeyPairs(self, bits: int = 1024) -> dict:
    p = self.rn.generateRandomPrimeNumber(bits)
    q = self.rn.generateRandomPrimeNumber(bits)
    n = p * q
    lambdaN = self.math.computeLambda(p, q)
    e = 65537 # usually fixed with it
    d = self.math.computeMultipicativeInverse(e, lambdaN)

    return { "sk": d, "vk": { "e": e, "n": n } }

  def sign(self, d: int, n: int, m: str) -> int:
    digest = sha256(self.utils.strToBytes(m)).hexdigest()
    digestDecimal = self.utils.hexStringToInt(digest)

    return pow(digestDecimal, d, n)

  def verify(self, vk: dict, signature: int, m: str) -> bool:
    e, n = vk["e"], vk["n"]
    digest = sha256(self.utils.strToBytes(m)).hexdigest()
    digestDecimal = self.utils.hexStringToInt(digest)
    hashValue = pow(signature, e, n)

    if digestDecimal == hashValue:
      return True
    else:
      return False