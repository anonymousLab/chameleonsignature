from fractions import gcd

from chameleon.randomness import RandomNumber



class Mathematics:
  def __init__(self):
    self.rn = RandomNumber()

  def computeLeastCommonMultiple(self, p: int, q: int, n: int) -> int:
    # derives from gcd(p, q) * lcm(p, q) = p * q
    # lcm(p, q) = (p * q) / gcd(p, q)
    return n // gcd(p, q)

  def computeLambda(self, p: int, q: int) -> int:
    # lambda(n) = lcm(p - 1, q - 1)
    p -= 1
    q -= 1
    return self.computeLeastCommonMultiple(p, q, p * q)

  def computeMultipicativeInverse(self, e: int, n: int) -> int:
    if self.rn.isPrime(n):
      # Fermat's little theorem
      # e^-1 = e^n-2 mod n
      return pow(e, n - 2, n)
    else:
      # Extended Euclidean Algorithm
      n0 = n
      p0 = 0
      p1 = 1

      while True:
        q = n // e # quotient
        r = n % e # remainder

        if not r:
          break
        else:
          n = e
          e = r

          p = p0
          p0 = p1
          p1 = pow(p - p1 * q, 1, n0)

      return p1

  def computeInverseDivision(self, a: int, x: int, n: int) -> int:
    _a = pow(a, 1, n)
    invX = self.computeMultipicativeInverse(x, n)

    try:
      invX < 0
    except:
      raise Exception("Division impossible.")
    else:
      return pow(_a * invX, 1, n)