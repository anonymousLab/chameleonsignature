from chameleon.randomness import RandomNumber
from chameleon.mathematics import Mathematics
from chameleon.utilities import Utilities
from chameleon.RSA import RSA



class Chameleon:
  def __init__(self):
    self.rn = RandomNumber()
    self.math = Mathematics()
    self.utils = Utilities()
    self.rsa = RSA()

  def computeParameters(self) -> dict:
    # Set prime p and q where p = kq + 1
    q = self.rn.generateRandomPrimeNumber(256)

    while True:
      k = self.rn.generateRandomInteger(768)
      p = k * q + 1
      if self.rn.isPrime(p):
        break

    # Schnorr group
    while True:
      h = self.rn.generateRandomInteger()
      if h < p:
        g = pow(h, k, p)
        if g != 1:
          break

    return { "p": p, "q": q, "g": g }

  def generateKeyPairs(self) -> dict:
    params = self.computeParameters()

    while True:
      x = self.rn.generateRandomInteger(256)
      if x < params["q"]:
        break

    y = pow(params["g"], x, params["p"])

    return { "ck": x, "hk": y, "params": params }

  def hash(self, hk: int, p: int, g: int, r: int, encodedM: int) -> int:
    # g ^ m (mod p)
    d1 = pow(g, encodedM, p)
    # g ^ (x * r)
    d2 = pow(hk, r, p)

    return pow(d1 * d2, 1, p)

  def sign(self, sk: int, n: int, hk: int, params: dict, encodedM: int, r = None) -> dict:
    p, q, g = params["p"], params["q"], params["g"]

    if r == None:
      while True:
        r = self.rn.generateRandomInteger(256)
        if r < q:
          break

    chameleonHash = self.hash(hk, p, g, r, encodedM)
    sig = self.rsa.sign(sk, n, self.utils.intToHexString(chameleonHash))

    return { "m": encodedM, "r": r, "sig": sig }

  def verify(self, vk: dict, hk: int, params: dict, signature: dict) -> bool:
    p, g = params["p"], params["g"]
    m, r, sig = signature["m"], signature["r"], signature["sig"]

    chameleonHash = self.hash(hk, p, g, r, m)

    if self.rsa.verify(vk, sig, self.utils.intToHexString(chameleonHash)):
      return True
    else:
      return False

  def collide(self, ck: int, params: dict, signature: dict, _m: str) -> dict:
    m, r = signature["m"], signature["r"]
    q = params["q"]

    _r = self.math.computeInverseDivision((m + ck * r - _m), ck, q)

    return { "_m": _m, "_r": _r }