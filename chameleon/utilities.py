class Utilities:
  def __init__(self):
    pass

  def bytesToBits(self, n: int) -> int:
    return n << 3

  def bitsToBytes(self, n: int) -> int:
    return n >> 3

  def hexStringToInt(self, s: str) -> int:
    return int(s, 16)

  def intToHexDecimal(self, n: int) -> str:
    return hex(n)

  def intToHexString(self, n: int) -> str:
    return str(self.intToHexDecimal(n))[2:]

  def bytesToInt(self, b: bytes) -> int:
    return int.from_bytes(b, byteorder='little')

  def intToBytes(self, n: int) -> bytes:
    return n.to_bytes((n.bit_length() + 7) // 8, byteorder='little')

  def strToBytes(self, s: str) -> bytes:
    return s.encode('utf-8')

  def strToInt(self, s: str) -> int:
    return self.bytesToInt(self.strToBytes(s))