from chameleon.randomness import RandomNumber
from chameleon.utilities import Utilities

from hashlib import sha256

class MessageEncoder():
  def __init__(self, group: int = 512):
    self.G = pow(2, group) - 1
    self.rn = RandomNumber()
    self.utils = Utilities()

  # Optimal Asymmetric Encryption Padding
  def encodeOAEP(self, message: str) -> int:
    byteMessage = message.encode('utf-8')
    intMessage = self.utils.bytesToInt(byteMessage)

    try:
      self.G // 2 > intMessage
    except:
      raise ValueError("message is too long.")
    else:
      halfBits = self.utils.bytesToBits(len(self.utils.intToBytes(self.G))) // 2
      zeroPadBits = halfBits - self.utils.bytesToBits(len(byteMessage))
      zeroPaddedMessage = intMessage << zeroPadBits
      r = self.rn.generateRandomInteger(halfBits)
      g = self.utils.hexStringToInt(sha256(self.utils.intToBytes(r)).hexdigest())
      x = zeroPaddedMessage ^ g
      h = self.utils.hexStringToInt(sha256(self.utils.intToBytes(x)).hexdigest())
      y = h ^ r

      return (x << halfBits) + y

  def decodeOAEP(self, message: int) -> str:
    halfBits = self.utils.bytesToBits(len(self.utils.intToBytes(self.G))) // 2
    x = message >> halfBits
    y = message - (x << halfBits)
    r = y ^ self.utils.hexStringToInt(sha256(self.utils.intToBytes(x)).hexdigest())
    m = self.utils.hexStringToInt(sha256(self.utils.intToBytes(r)).hexdigest()) ^ x

    return self.utils.intToBytes(m).decode('utf-8')