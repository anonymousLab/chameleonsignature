# Chameleon Signatures
This code is built under Krawczyk and Rabin as below:
Hugo Krawczyk and Tal Rabin, Chameleon Signature, NDSS 2000

## Chameleon Hashing
A **chameleon hash** is interestingly generated with a pair of private/public key(which is a trapdoor).
Some properties as below(from the paper):
1. Anyone who knows the public key can compute the associated hash function.
2. For those who don't know the trapdoor the function is collision resistant in unusual sense, namely, it is infeasible to find two inputs which are mapped to the same output.
3. However, the holder of the trapdoor key can easily find collisions for every given input.

### Chameleon Hash
(Please check the paper in detail)
CHAM-HASH(m, r) = g^m * y^r mod p

## Chameleon Signature
The introduced hash function in this paper on which a usually using digital signature is applied.
Some properties as below(from the paper):
1. Non-repudiation - As in regular digital signatures, the signer _S_ cannot repudiate (or deny) a signature he generated since he cannot find collisions in the hash.
2. Non-transferable - The recipient cannot prove to any third party that a given signature of _S_ corresponds to a certain message since _R_ could "open" the signed message in any way he wants as he can find collision using the trapdoor information of the hash.
3. Recipient-specific - If the same message is intended for two different recipients then the signer needs to sign it twice, once for each recipient (since the chameleon hash fucntions are specific and different for each recipient).

### Chameleon Signature Generation Scheme
Message _m_
private signing key of _S_, SK
HK of _R_, i.e. HK = y(= g^x mod p), g, p, q

1. Generate the chame hash by choosing a random r; computing hash = CHAM-HASH(m, r) = g^m * y^r mod p
2. Set sig = SIGN(hash, HK)
3. The signature on the message _m_ consists of SIG(m) = (m, r, sig)

### Chameleon Signature Verification
SIG(m) = (m, r, sig)
public verification key of _S_, VK
HK of _R_, i.e. HK = y, g, p, q

1. Compute hash = CHAM-HASH(m, r) = g^m * y^r mod p
2. output = **proper** verify((hash, HK), sig) = valid
3. output = **improper** otherwise

## Collisions
A forgery SIG(m') = (m', r', sig)

1. _S_ retrieves the original values m, r used to compute sig. It holds that g^m' * y^r' mod p, while m != m'
2. _S_ computes x = ((m - m') / (r' - r)) mod p
3. _s_ chooses any message m'' and computes r'' = ((m + x * r - m'') / x) mod q
4. Output (m'', r'')

## Enhancement to the Basic Scheme
Please check the chapter 4.2 on the paper.
